
package com.bogo.webhook.feign;

import com.bogo.webhook.feign.request.TulingRequest;
import com.bogo.webhook.feign.response.TulingResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(value = "tuling-service",url = "http://openapi.tuling123.com")
public interface TulingRobotService {

	@PostMapping(path = "/openapi/api/v2")
	TulingResponse get(@RequestBody TulingRequest request);
}
