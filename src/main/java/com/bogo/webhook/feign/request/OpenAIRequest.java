package com.bogo.webhook.feign.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;

public class OpenAIRequest {

    private final String model = "gpt-3.5-turbo";

    @JsonProperty("max_tokens")
    private final int maxTokens = 2048;

    @JsonProperty("frequency_penalty")
    private final int frequencyPenalty = 0;

    @JsonProperty("presence_penalty")
    private final int presencePenalty = 0;

    private final float temperature = 0.8F;

    private final List<Message> messages = new LinkedList<>();

    public void setContent(String content){
        Message message = new Message();
        message.content = content;
        messages.add(message);
    }

    public List<Message> getMessages() {
        return messages;
    }

    public String getModel() {
        return model;
    }

    public int getMaxTokens() {
        return maxTokens;
    }

    public int getFrequencyPenalty() {
        return frequencyPenalty;
    }

    public int getPresencePenalty() {
        return presencePenalty;
    }

    public float getTemperature() {
        return temperature;
    }



    public static class Message{
        private final String role = "user";
        private String content;

        public String getRole() {
            return role;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
