package com.bogo.webhook.controller;

import com.bogo.webhook.controller.request.QuoteRequest;
import com.bogo.webhook.feign.ChatGptBotService;
import com.bogo.webhook.feign.BogoRobotService;
import com.bogo.webhook.feign.request.OpenAIRequest;
import com.bogo.webhook.feign.request.RobotMessageRequest;
import com.bogo.webhook.service.RobotChatService;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 和信群机器人webhook实现
 */
@RestController
public class RobotController {


	@Resource
	private RobotChatService robotChatService;

	@Resource
	private BogoRobotService bogoRobotService;

	@Resource
	private ChatGptBotService chatGptBotService;

	@PostMapping(value = "/robot")
	public ResponseEntity<Void> handle(@RequestBody QuoteRequest request) {

		String text = robotChatService.get(request.getContent(),request.getUuid());

		RobotMessageRequest messageRequest = new RobotMessageRequest();

		messageRequest.setContent(text);

		messageRequest.setMessageId(request.getMessageId());

		bogoRobotService.send(request.getUuid(),messageRequest);

		return ResponseEntity.ok().build();
	}

	@PostMapping(value = "/chat-gpt")
	public ResponseEntity<Void> gptChat(@RequestBody QuoteRequest request) {

		OpenAIRequest completionRequest = new OpenAIRequest();
		completionRequest.setContent(request.getContent());

		String text = chatGptBotService.get(completionRequest ).getText();

		if (StringUtils.hasText(text)) {

			RobotMessageRequest messageRequest = new RobotMessageRequest();

			messageRequest.setContent(text);

			messageRequest.setMessageId(request.getMessageId());

			bogoRobotService.send(request.getUuid(), messageRequest);
		}

		return ResponseEntity.ok().build();
	}

}
