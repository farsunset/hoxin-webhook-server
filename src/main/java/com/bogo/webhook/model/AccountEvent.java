 
package com.bogo.webhook.model;

import java.io.Serializable;

/**
 *公众账号操作事件
 */
public class AccountEvent implements Serializable{
 
	public static final long serialVersionUID = 1L;

	public final static String ACTION_TEXT = "text";//发送文字事件
	public final static String ACTION_MENU = "menu";//点击菜单事件

	public final static String ACTION_SUBSCRIBE = "subscribe";//订阅事件
	public final static String ACTION_UNSUBSCRIBE = "unsubscribe";//取消订阅事件

	/*
	 */
	public String action;

	/*
    用户发送的事件内容
     */
	public String content;

	/*
	 *用户id
	 */
	public String uid;


}
