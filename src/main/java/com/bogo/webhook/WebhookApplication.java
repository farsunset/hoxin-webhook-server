package com.bogo.webhook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@EnableFeignClients
@SpringBootApplication
public class WebhookApplication extends MappingJackson2HttpMessageConverter {
	public static void main(String[] args) {
		SpringApplication.run(WebhookApplication.class, args);
	}


}
